*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported Selenium2Library.
Library           Selenium2Library

*** Variables ***
${SERVER}		SERV_ADDR
${BROWSER_1}		firefox
${BROWSER_2}		chrome
${DELAY}		0.2
${MAIN_PAGE URL}	http://${SERVER}/
${MENU_BUTTON}		btn-open

*** Keywords ***
Open Chrome Browser To Main Page
    Open Browser		${MAIN_PAGE URL}    ${BROWSER_2}
    Set Selenium Speed		${DELAY}
Open Firefox Browser To Main Page
    Open Browser		${MAIN_PAGE URL}    ${BROWSER_1}
    Set Selenium Speed		${DELAY}
Click Menu Button
    Click Element		${MENU_BUTTON}
