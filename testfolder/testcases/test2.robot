*** Settings ***
Documentation     Test expandable menu using firefox
...
...               This test has a workflow that is created using keywords in
...               the imported resource file.
Resource          /home/root/testing/testfolder/resources/resource.robot

*** Variables ***


*** Test Cases ***
Title Should Be Iotitude
    Open Firefox Browser To Main Page
    Location Should Be		${MAIN_PAGE URL}
    Title Should Be		Iotitude
Chart Element Test
    Click Menu Button
    Click Element                       chart-select
    Click Element                       chart-select-pie
    Page Should Contain Element         pie-chart
    Click Element                       chart-select
    Click Element                       chart-select-bar
    Page Should Contain Element         bar-chart
    Click Element                       chart-select
    Click Element                       chart-select-doughnut
    Page Should Contain Element         doughnut-chart
    Click Element                       chart-select
    Click Element                       chart-select-polar
    Page Should Contain Element         polar-chart
Time select Element Test
    Click Element               time-select
    Click Element               time-select-week
    Page Should Contain         Last 7 days
    Click Element               time-select
    Click Element               time-select-day
    Page Should Contain         Today
    Click Element               time-select
    Click Element               time-select-month
    Page Should Contain         Last 30 days
Limit select test
    Click Element		        limit-select
    Click Element		        limit-select-10
    Page Should Contain	    	10
    Click Element	        	limit-select
    Click Element	        	limit-select-1000
    Page Should Contain	    	1 000
    Click Element	        	limit-select
    Click Element		        limit-select-100000
    Page Should Contain	    	100 000
Endpoint select test
    Click Element               endpoint-select
    Click Element               enpoint-select-1
    Page Should Contain         1
    Click Element               endpoint-select
    Click Element               enpoint-select-5
    Page Should Contain         5
Color selector tests
    Click Element               color1-select
    Click Element               color-select-blue
    Page Should Contain         Blue
    Click Element               color2-select
    Click Element               color-select-lime
    Page Should Contain         Lime
    Click Element               color3-select
    Click Element               color-select-dpurple
    Page Should Contain         Deep Purple
    [Teardown]                  Close All Browsers
